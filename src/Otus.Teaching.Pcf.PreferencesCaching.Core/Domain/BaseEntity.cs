﻿namespace Otus.Teaching.Pcf.PreferencesCaching.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
