﻿namespace Otus.Teaching.Pcf.PreferencesCaching.Core.Domain
{
    public class Preference
        : BaseEntity
    {
        public string Name { get; set; }
    }
}
