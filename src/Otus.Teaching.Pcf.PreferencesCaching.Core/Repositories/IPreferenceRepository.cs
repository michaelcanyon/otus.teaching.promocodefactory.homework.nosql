﻿using Otus.Teaching.Pcf.PreferencesCaching.Core.Domain;

namespace Otus.Teaching.Pcf.PreferencesCaching.Core.Repositories
{
    public interface IPreferenceRepository : IRepository<Preference>
    {
        Task DeleteBatchByIdsAsync(IEnumerable<Guid> ids);
    }
}
