﻿using Microsoft.EntityFrameworkCore.Internal;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            //_dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
            if (!_dataContext.Partners.Any())
                _dataContext.AddRange(FakeDataFactory.Partners);
            _dataContext.SaveChanges();
        }
    }
}