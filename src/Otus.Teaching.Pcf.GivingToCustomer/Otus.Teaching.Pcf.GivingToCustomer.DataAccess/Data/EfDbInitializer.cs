﻿using System.Linq;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            //_dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            if (!_dataContext.Customers.Any())
                _dataContext.AddRange(FakeDataFactory.Customers);

            _dataContext.SaveChanges();
        }
    }
}