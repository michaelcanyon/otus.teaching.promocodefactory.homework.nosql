﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    public class PreferenceDTO
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
