﻿using Otus.Teaching.Pcf.PreferencesCaching.DataAccess.Data;

namespace Otus.Teaching.Pcf.PreferencesCaching.Middlewares
{
    public class InitialDb
    {
        private readonly RequestDelegate _next;

        public InitialDb(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, IDbInitializer dbInitializer)
        {
            dbInitializer.InitializeDb();
            await _next(context);
        }
    }

    public static class InitialDbExtension
    {
        public static IApplicationBuilder UseInitialDbExtension(this IApplicationBuilder builder)
            => builder.UseMiddleware<InitialDb>();
    }
}
