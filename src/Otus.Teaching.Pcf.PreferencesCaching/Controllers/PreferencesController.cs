﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.PreferencesCaching.Core.Domain;
using Otus.Teaching.Pcf.PreferencesCaching.Core.Repositories;
using Otus.Teaching.Pcf.PreferencesCaching.DTO;
using StackExchange.Redis;
using System.Runtime.CompilerServices;
using System.Text.Json;

namespace Otus.Teaching.Pcf.PreferencesCaching.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreferencesController : ControllerBase
    {
        private readonly IPreferenceRepository _preferenceRepository;

        private readonly IDatabaseAsync _cache;

        public PreferencesController(IPreferenceRepository preferenceRepository, IConnectionMultiplexer cache)
        {
            _preferenceRepository = preferenceRepository ?? throw new ArgumentNullException(nameof(preferenceRepository));
            _cache = cache.GetDatabase() ?? throw new ArgumentNullException(nameof(cache));
        }

        [HttpGet("GetSingleById")]
        public async Task<ActionResult<Preference>> GetSingleById([FromQuery]Guid id)
        {
            var preferenceString = await _cache.StringGetAsync(id.ToString());
            var preference = string.IsNullOrEmpty(preferenceString)
                ? null
                : JsonSerializer.Deserialize<Preference>(preferenceString);
            if (preference == null)
            {
                var dbPreference = await _preferenceRepository.GetFirstWhere(p => p.Id == id);
                if (dbPreference == null)
                    return NotFound("Предпочтение не найдено по указанному ключу");
                var serializedPrefrence = JsonSerializer.Serialize(dbPreference);
                await _cache.StringSetAsync(id.ToString(), serializedPrefrence, TimeSpan.FromSeconds(30));
                return Ok(dbPreference);
            }
            return Ok(preference);
        }

        [HttpGet("GetByIds")]
        public async Task<ActionResult<List<Preference>>> GetByIds([FromQuery]IEnumerable<Guid> ids)
        {
            var stringArray = ids.Select(id => (RedisKey)id.ToString()).ToArray();
            var preferenceStrings = await _cache.StringGetAsync(stringArray);
            var preferences = preferenceStrings
                .Where(s => !s.IsNullOrEmpty)
                .Select(s => JsonSerializer.Deserialize<Preference>(s))?
                .ToList();
            if (preferences == null)
                throw new ArgumentNullException(nameof(preferences));

            var notFoundPreferenceIds = ids.Except(preferences.Select(p => p.Id)).ToList();
            var notFoundPreferences = new List<Preference>();
            if (notFoundPreferenceIds.Count > 0)
            {
                var dbPreferences = await _preferenceRepository.GetRangeByIdsAsync(notFoundPreferenceIds);
                if (!dbPreferences.Any())
                    return Ok(preferences);
                preferences.AddRange(dbPreferences);
                var cacheTasks = dbPreferences.Select(p 
                    => _cache.StringSetAsync(p.Id.ToString(),
                                             JsonSerializer.Serialize(p),
                                             TimeSpan.FromSeconds(30)));
                await Task.WhenAll(cacheTasks);
            }
            return Ok(preferences);
        }

        [HttpDelete("DeleteByIds")]
        public async Task<ActionResult<bool>> DeleteByIds([FromQuery]IEnumerable<Guid> ids)
        {
            if (!ids.Any())
                return BadRequest();

           await _preferenceRepository.DeleteBatchByIdsAsync(ids);
           await _cache.KeyDeleteAsync(ids.Select(k=>(RedisKey)k.ToString()).ToArray());
           return Ok(true);
        }

        [HttpPost("CreateNew")]
        public async Task<ActionResult<Guid>> CreateNew([FromQuery] PreferenceDTO preference)
        {
            var preferenceModel = new Preference { Name = preference.Name, Id = Guid.NewGuid() }; 
            await _preferenceRepository.AddAsync(preferenceModel);
            await _cache.StringSetAsync(preferenceModel.Id.ToString(),
                                        JsonSerializer.Serialize(preferenceModel),
                                        TimeSpan.FromSeconds(30));
            return Ok(preferenceModel.Id);
        }

        [HttpPost("UpdatePreference")]
        public async Task<ActionResult<Preference>> UpdatePreference([FromQuery] Preference preference)
        {
            await _preferenceRepository.UpdateAsync(preference);
            await _cache.StringSetAsync(preference.Id.ToString(),
                                        JsonSerializer.Serialize(preference),
                                        TimeSpan.FromSeconds(30));
            return Ok(preference);
        }

        [HttpGet("GetAll")]
        public async Task<ActionResult<List<Preference>>> GetAll()
            => Ok(await _preferenceRepository.GetAllAsync());
    }
}
