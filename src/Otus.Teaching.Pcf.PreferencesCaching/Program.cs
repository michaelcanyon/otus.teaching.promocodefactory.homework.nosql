using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.PreferencesCaching.Core.Repositories;
using Otus.Teaching.Pcf.PreferencesCaching.DataAccess;
using Otus.Teaching.Pcf.PreferencesCaching.DataAccess.Data;
using Otus.Teaching.Pcf.PreferencesCaching.DataAccess.Repositories;
using StackExchange.Redis;
using Microsoft.AspNetCore.Builder;
using Otus.Teaching.Pcf.PreferencesCaching.Middlewares;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddTransient(typeof(IRepository<>), typeof(EfRepository<>));
builder.Services.AddTransient<IPreferenceRepository, PreferenceRepository>();
builder.Services.AddScoped<IDbInitializer, EfDbInitializer>();

builder.Services.AddDbContext<DataContext>(options =>
{
    var config = builder.Configuration.GetValue<string>("PostgresDatabase:ConnectionString");
    options.UseNpgsql(config);
});

builder.Services.AddSingleton<IConnectionMultiplexer>(sp 
    => ConnectionMultiplexer.Connect(builder.Configuration.GetValue<string>("Redis:ConnectionString")));

builder.Services.AddOpenApiDocument(options => {
    options.Title = "Promocode factory caching service";
    options.Version = "v1.0";
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    //app.UseHsts();
}

app.UseOpenApi();
app.UseSwaggerUi3(x =>
{
    x.DocExpansion = "list";
});

//app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllers();

app.UseInitialDbExtension();

app.Run();


