﻿namespace Otus.Teaching.Pcf.PreferencesCaching.DTO
{
    public record PreferencesDTO
    {
        public IEnumerable<PreferenceDTO>? Preferences { get; init; }

        public IEnumerable<Guid>? NotFoundPreferenceIds { get; init; }
    }
}
