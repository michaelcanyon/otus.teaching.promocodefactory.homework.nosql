﻿namespace Otus.Teaching.Pcf.PreferencesCaching.DTO
{
    public class PreferenceDTO
    {
        public string Name { get; init; }
    }
}
