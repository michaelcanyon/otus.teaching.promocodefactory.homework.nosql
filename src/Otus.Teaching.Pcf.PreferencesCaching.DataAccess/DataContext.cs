﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.PreferencesCaching.Core.Domain;

namespace Otus.Teaching.Pcf.PreferencesCaching.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Preference> Preferences { get; set; }

        public DataContext()
        {
            Database.Migrate();
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseNpgsql("Host=localhost;Port=5433;Database=usersdb;Username=postgres;Password=qwertyqwerty");
        //}

    }
}
