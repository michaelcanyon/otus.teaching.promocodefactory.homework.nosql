﻿namespace Otus.Teaching.Pcf.PreferencesCaching.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}
