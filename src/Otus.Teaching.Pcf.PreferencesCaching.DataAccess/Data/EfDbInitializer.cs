﻿using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.Pcf.PreferencesCaching.DataAccess.Data
{
    public class EfDbInitializer : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            //_dataContext.Database.EnsureDeleted();
            //_dataContext.Database.EnsureCreated();
            
            if (_dataContext.Database.GetPendingMigrations().Any())
            {
                _dataContext.Database.Migrate();

                _dataContext.AddRange(FakeDataFactory.Preferences);
                _dataContext.SaveChanges();
            }
        }
    }
}
