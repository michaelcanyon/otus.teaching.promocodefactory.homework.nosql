﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.PreferencesCaching.Core.Domain;
using Otus.Teaching.Pcf.PreferencesCaching.Core.Repositories;

namespace Otus.Teaching.Pcf.PreferencesCaching.DataAccess.Repositories
{
    public class PreferenceRepository : EfRepository<Preference>, IPreferenceRepository
    {
        private readonly DataContext _dataContext;

        public PreferenceRepository(DataContext dataContext) : base(dataContext)
            =>_dataContext = dataContext;

        public async Task DeleteBatchByIdsAsync(IEnumerable<Guid> ids)
        {
            var entities = await _dataContext.Preferences.Where(p => ids.Contains(p.Id))
                                                         .ToListAsync();
            if (entities.Any())
            {
                _dataContext.Preferences.RemoveRange(entities);
                await _dataContext.SaveChangesAsync();
            }
        }
    }
}
